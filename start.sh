#!/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source "${SCRIPT_DIR}/venv/bin/activate"

while true; do python "${SCRIPT_DIR}/sensor.py" --print ; sleep 30; done
