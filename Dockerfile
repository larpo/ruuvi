FROM python:3-slim


RUN set -x \
    && apt-get update && apt-get install -y python3-dev sudo bluez bluez-hcidump \
    && apt-get purge -y

WORKDIR /code

ADD requirements.txt /code/

RUN pip install -r /code/requirements.txt

ADD *.py /code/
CMD ["python", "-u", "sensor.py"]
