#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

from ruuvitag_sensor.ruuvi import RuuviTagSensor

import socket
import json
import os
import time
import threading
import paho.mqtt.client as paho
import argparse

# Parse command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("--print", action="store_true", help="Enable printing")
args = parser.parse_args()

latest = dict()
configured_devices = set()
hostname = socket.gethostname()

mqtt = paho.Client("ruuvi-publisher-%s" % hostname)
mqtt.connect(os.getenv("MQTT_IP", "192.168.1.2"))
mqtt.loop_start()  # start the network loop so that keepalive messages etc get handled

print("starting up ruuvi-publisher-%s!" % hostname)


def send_config(device):
    global mqtt, configured_devices
    temp = {
        'name': f'Ruuvi {device}',
        'unique_id': f'ruuvi-{device}-temp',
        'object_id': f'ruuvi-{device}-temp',
        'state_topic': f'ruuvi/{device}/temperature',
        'device_class': 'temperature',
        'unit_of_measurement': '°C',
        'suggested_display_precision': 1,
        'expire_after': 5 * 60,
    }

    hum = {
        'name': f'Ruuvi {device}',
        'unique_id': f'ruuvi-{device}-hum',
        'object_id': f'ruuvi-{device}-hum',
        'state_topic': f'ruuvi/{device}/humidity',
        'device_class': 'humidity',
        'unit_of_measurement': '%',
        'suggested_display_precision': 0,
        'expire_after': 5 * 60,
    }

    mqtt.publish(f'homeassistant/sensor/ruuvi-{device}-temp/config', json.dumps(temp), retain=True)
    mqtt.publish(f'homeassistant/sensor/ruuvi-{device}-hum/config', json.dumps(hum), retain=True)
    configured_devices.add(device)


last_updates = {}
last_values = {}
last_error = None


def handle_data(found_data):
    global mqtt, configured_devices, last_updates, last_values, last_error

    mac, data = found_data
    device = "".join(mac.split(":")[-2:])
    temp = round(data["temperature"], 2)
    hum = round(data["humidity"], 2)

    try:
        if device not in configured_devices:
            send_config(device)

        current_time = time.time()
        last_update = last_updates.get(device, 0)

        if current_time - last_update >= 10:
            mqtt.publish("ruuvi/%s/temperature" % device, temp)
            mqtt.publish("ruuvi/%s/humidity" % device, hum)

            last_updates[device] = current_time
            last_values[device] = (temp, hum)
    except Exception as e:
        last_error = str(e)


def print_latest_values():
    global last_updates, last_values, last_error

    while True:
        if args.print:
            os.system('clear')

            current_time = time.time()

            for device, (temp, hum) in last_values.items():
                seconds_ago = current_time - last_updates.get(device, 0)
                print(f"Device {device} last temperature: {temp} (updated {seconds_ago:.1f} seconds ago)")
                # print(f"Device {device} last humidity: {hum} (updated {seconds_ago:.1f} seconds ago)")
            if last_error:
                print(f"Last error: {last_error}")

        time.sleep(1)


if args.print:
    print_thread = threading.Thread(target=print_latest_values)
    print_thread.daemon = True  # Set thread as daemon so that it doesn't throw error when exiting
    print_thread.start()

RuuviTagSensor.get_datas(handle_data)
